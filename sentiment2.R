library(wordcloud)
library(twitteR)
library(ROAuth)
library(tm)
library(sentiment)
library(ggplot2)

# --------------- TWITTER CREDENDIALS AND TWEET EXTRACTION ----------------
apiKey <- 'OY9AlUG1fM49rytP7T8hqInoL'
apiSecret <- 'cjKBprKRaZoUUvwPUv7MX3NeRCehn8dSs9vBOwGjsbvAXBjI9s'
access_token <- '2195494530-V7csBiOqiffi69fZuhkZEqMRZO5xqHHWcSiBl9V'
access_token_secret <- 'CQ96GaCgeIFqq2XVPtigJnFVD9momtZqFBEsHpH8n7q3Z'

#setting up twitter third party authentication
setup_twitter_oauth(apiKey,apiSecret,access_token,access_token_secret)

#hashtags to be searched
hashtags <- c("#indianfood")
searchString <- paste(hashtags, collapse = " OR ")
#getting tweets
tweets <- searchTwitter(searchString,n=200,"en")

tweets.df <- twListToDF(tweets)

myCorpus <- Corpus(VectorSource(tweets.df$text)) #corpus for documents

# -----------CLEANING THE TWEETS--------------

# remove url
removeURL <- function(x) gsub("http[^[:space:]]*", "", x)
myCorpus <- tm_map(myCorpus, content_transformer(removeURL))

# remove anything other than English letters or space
removeNumPunct <- function(x) gsub("[^[:alpha:][:space:]]*", "", x)
myCorpus <- tm_map(myCorpus, content_transformer(removeNumPunct))

# convert to lower case
myCorpus <- tm_map(myCorpus, content_transformer(tolower))

# remove stopwords
myStopwords <- c(setdiff(stopwords('english'), c("r", "big")),"use", "see", "used", "via", "amp")
myCorpus <- tm_map(myCorpus, removeWords, myStopwords)

# remove extra whitespace
myCorpus <- tm_map(myCorpus, stripWhitespace)

myCorpusCopy <- myCorpus
myCorpus <- tm_map(myCorpus,stemDocument) #stem words

stemCompletion2 <- function(x,dictionary) {
  x <- unlist(strsplit(as.character(x)," "))
  x <- x[x != ""]
  x <- stemCompletion(x,dictionary=dictionary)
  x <- paste(x,sep="",collapse=" ")
  PlainTextDocument(stripWhitespace(x))
}

myCorpus <- lapply(myCorpus,stemCompletion2,dictionary=myCorpusCopy)
myCorpus <- Corpus(VectorSource(myCorpus))

# count word Frequency
wordFreq <- function(corpus,word) {
  results <- lapply(corpus, function(x) {
    grep(as.character(x), pattern=paste("\\<", word))
  })
  sum(unlist(results))
}

# replace old word with new word
replaceWord <- function(corpus,oldword,newword) {
  tm_map(corpus,content_transformer(gsub),pattern=oldword,replacement=newword)
}
myCorpus <- replaceWord(myCorpus,"universidad","university")
myCorpus <- replaceWord(myCorpus, "scienc", "science")
myCorpus <- replaceWord(myCorpus,"character","")
myCorpus <- replaceWord(myCorpus,"list","")
myCorpus <- replaceWord(myCorpus,"0","")

#----------------- SEGREGATION OF TWEETS BASED ON FOOD PRODUCTS ----------------

# problem occurs here when trying to separate

# ----------------FREQUENT WORDS PART------------------
tdm <- TermDocumentMatrix(myCorpus,control = list(wordLengths = c(1, Inf)))

# term.freq <- rowSums(as.matrix(tdm))
# term.freq <- subset(term.freq, term.freq >= 20)
# df <- data.frame(term = names(term.freq), freq = term.freq)
# ggplot(df, aes(x=term, y=freq)) + geom_bar(stat="identity") + xlab("Terms") + ylab("Count") + coord_flip() + theme(axis.text=element_text(size=7))

m <- as.matrix(tdm)
word.freq <- sort(rowSums(m),decreasing=T)
pal <- brewer.pal(9,"BuGn")[-(1:4)]

# plotting word cloud of frequent terms
wordcloud(words = names(word.freq), freq = word.freq, min.freq = 3,random.order = F, colors = pal)

# ------------SENTIMENT ANALYSIS PART--------------
sentiments <- sentiment(tweets.df$text)
table(sentiments$polarity)

# sentiment plot
sentiments$score <- 0
sentiments$score[sentiments$polarity == "positive"] <- 1
sentiments$score[sentiments$polarity == "negative"] <- -1

