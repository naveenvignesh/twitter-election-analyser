library(twitteR)
library(ROAuth)
library(tm)
library(xlsx)
library(plyr)
library(stringr)

#Twitter Credentials
apiKey <- 'OY9AlUG1fM49rytP7T8hqInoL'
apiSecret <- 'cjKBprKRaZoUUvwPUv7MX3NeRCehn8dSs9vBOwGjsbvAXBjI9s'
access_token <- '2195494530-V7csBiOqiffi69fZuhkZEqMRZO5xqHHWcSiBl9V'
access_token_secret <- 'CQ96GaCgeIFqq2XVPtigJnFVD9momtZqFBEsHpH8n7q3Z'

#setting up twitter third party authentication
setup_twitter_oauth(apiKey,apiSecret,access_token,access_token_secret)

#method to remove url from tweet text
removeURL <- function(x) gsub("http[[:alnum:][:punct:]]*", "", x)

#twitter search parameters
searchString <- c('#indianfood')
no <- 100
lang <- "en"

#Method to score sentiment values
score.sentiment = function(tweets, pos.words, neg.words,.progress='none')
{
  require(plyr)
  require(stringr)

  scores = laply(tweets, function(tweet, pos.words, neg.words) {

    tweet = gsub('https://','',tweet) # removes https://
    tweet = gsub('http://','',tweet) # removes http://
    tweet=gsub('[^[:graph:]]', ' ',tweet) ## removes graphic characters
    #like emoticons
    tweet = gsub('[[:punct:]]', '', tweet) # removes punctuation
    tweet = gsub('[[:cntrl:]]', '', tweet) # removes control characters
    tweet = gsub('\\d+', '', tweet) # removes numbers
    tweet=str_replace_all(tweet,"[^[:graph:]]", " ")

    tweet = tolower(tweet) # makes all letters lowercase

    word.list = str_split(tweet, '\\s+') # splits the tweets by word in a list

    words = unlist(word.list) # turns the list into vector

    pos.matches = match(words, pos.words) ## returns matching
    #values for words from list
    neg.matches = match(words, neg.words)

    pos.matches = !is.na(pos.matches) ## converts matching values to true of false
    neg.matches = !is.na(neg.matches)

    score = sum(pos.matches) - sum(neg.matches) # true and false are
    #treated as 1 and 0 so they can be added

    return(score)

  }, pos.words, neg.words )

  scores.df = data.frame(score=scores, text=tweets)

  return(scores.df)

} #end of sentiment method

#positive and negative text dictionary
pos <- scan('e:/RTWitterAnalyser/positive-words.txt',what='character',comment.char = ';')
neg <- scan('e:/RTWitterAnalyser/negative-words.txt',what='character',comment.char = ';')

#extracting tweets from online
tweets <- searchTwitter(searchString,no,lang)

#converting tweets to data frame
Tweets.text = laply(tweets,function(t)t$getText()) # gets text from Tweets

food.text = subset(Tweets.text,grep("rice",Tweets.text))

print(food.text)
analysis = score.sentiment(Tweets.text, pos, neg) # calls sentiment function